﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using ControlDatos.RegistroAccesoTableAdapters;

namespace WebService
{
    /// <summary>
    /// Descripción breve de Acceso
    /// </summary>
    [WebService(Namespace = "http://veterinapp.ml/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class Acceso : System.Web.Services.WebService
    {
       

        [WebMethod(EnableSession = true)]
        public DataTable ControlUsuarios(string Nit, string Identificacion, string Usuario, string Contrasena)
        {
            AccesoDataTableAdapter Acceso = new AccesoDataTableAdapter();

            DataTable dtUsuario = Acceso.Acceso(Nit,Identificacion,Usuario,Contrasena);

            if (dtUsuario == null)
            {
                return null;
            }
            else if (dtUsuario.Rows.Count == 0)
            {
                return null;
            }
            else
            {
                return dtUsuario;           
            }            
        }
    }
}
