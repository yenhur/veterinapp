﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="acceso.aspx.cs" Inherits="webVeterinapp._01_acceso.acceso" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- BEGIN: Head-->

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0" />
    <meta name="description" content="Aplicación Web para la gestión de atenciónes veterinarias" />
    <meta name="keywords" content="veterinaria, " />
    <meta name="author" content="PIXINVENT" />
    <title>Acceso a VeterinApp</title>
    <link rel="apple-touch-icon" href="../../../app-assets/images/ico/apple-icon-120.png" />
    <link rel="shortcut icon" type="image/x-icon" href="../../../app-assets/images/ico/icono_.ico" />
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet" />


    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/vendors.min.css" />
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="../../../app-assets/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="../../../app-assets/css/bootstrap-extended.min.css" />
    <link rel="stylesheet" type="text/css" href="../../../app-assets/css/colors.min.css" />
    <link rel="stylesheet" type="text/css" href="../../../app-assets/css/components.min.css" />
    <link rel="stylesheet" type="text/css" href="../../../app-assets/css/themes/dark-layout.min.css" />
    <link rel="stylesheet" type="text/css" href="../../../app-assets/css/themes/semi-dark-layout.min.css" />
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="../../../app-assets/css/core/menu/menu-types/horizontal-menu.min.css" />
    <link rel="stylesheet" type="text/css" href="../../../app-assets/css/pages/authentication.css" />
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="../../../assets/css/style.css" />
    <!-- END: Custom CSS-->
</head>
<body class="horizontal-layout horizontal-menu navbar-static dark-layout 1-column   footer-static bg-full-screen-image  blank-page blank-page" data-open="hover" data-menu="horizontal-menu" data-col="1-column" data-layout="dark-layout">
    <form runat="server" id="FormLogin">
        <!-- BEGIN: Content-->
        <div class="app-content content">
            <div class="content-overlay"></div>
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <!-- login page start -->
                    <section id="auth-login" class="row flexbox-container">
                        <div class="col-xl-8 col-11">
                            <div class="card bg-authentication mb-0">
                                <div class="row m-0">
                                    <!-- left section-login -->
                                    <div class="col-md-6 col-12 px-0">
                                        <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                                            <div class="card-header pb-1">
                                                <div class="card-title">
                                                    <h4 class="text-center mb-2">Bienvenido a VeterinApp</h4>                                                  
                                                </div>
                                            </div>
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <div class="d-flex flex-md-row flex-column justify-content-around">
                                                        <a href="#"
                                                            class="btn btn-social btn-google btn-block font-small-3 mr-md-1 mb-md-0 mb-1">
                                                            <i class="bx bxl-google font-medium-3"></i><span
                                                                class="pl-50 d-block text-center">Google</span></a>
                                                        <a href="#" class="btn btn-social btn-block mt-0 btn-facebook font-small-3">
                                                            <i class="bx bxl-facebook-square font-medium-3"></i><span
                                                                class="pl-50 d-block text-center">Facebook</span></a>
                                                    </div>
                                                    <div class="divider">
                                                        <div class="divider-text text-uppercase text-muted">
                                                            <small>o ingrese con usuario y contraseña</small>
                                                        </div>
                                                    </div>
                                                         <div class="form-group mb-50">
                                                            <label class="text-bold-600" for="exampleInputEmail1">Numero único de identificación tributaria (NIT)</label>
                                                            <input type="number" class="form-control" id="txtNitVeterinaria"
                                                                placeholder="Numero único de identificación tributaria">
                                                        </div>
                                                        <div class="form-group mb-50">
                                                            <label class="text-bold-600" for="exampleInputEmail1">Correo electrónico</label>
                                                            <input type="email" class="form-control" id="exampleInputEmail1"
                                                                placeholder="Correo electrónico">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-bold-600" for="exampleInputPassword1">Contraseña</label>
                                                            <input type="password" class="form-control" id="exampleInputPassword1"
                                                                placeholder="Contraseña">
                                                        </div>
                                                        <div
                                                            class="form-group d-flex flex-md-row flex-column justify-content-between align-items-center">
                                                            <div class="text-left">
                                                                <div class="checkbox checkbox-sm">
<%--                                                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                                    <label class="checkboxsmall" for="exampleCheck1">
                                                                        <small>Keep me logged
                                                        in</small></label>--%>
                                                                </div>
                                                            </div>
                                                            <div class="text-right">
                                                                <a href="auth-forgot-password.html"
                                                                    class="card-link"><small>Recuperar mi contraseña?</small></a>
                                                            </div>
                                                        </div>
                                                        <asp:Button runat="server" id="btnIngresar" Text="Ingresar" OnClick="btnIngresar_Click" type="submit" class="btn btn-primary glow w-100 position-relative">
                                                            </asp:Button>
                                                    
                                                    <hr>
                                                    <div class="text-center">
                                                        <small class="mr-25">No tengo una cuenta,</small><a
                                                            href="auth-register.html"><small style="color:white">Solicitar</small></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- right section image -->
                                    <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
                                        <div class="card-content">
                                            <img class="img-fluid" src="../../../app-assets/images/pages/login.png" alt="branding logo">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- login page ends -->

                </div>
            </div>
        </div>
        <!-- END: Content-->


        <!-- BEGIN: Vendor JS-->
        <script src="../../../app-assets/vendors/js/vendors.min.js"></script>
        <script src="../../../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="../../../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.min.js"></script>
        <script src="../../../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="../../../app-assets/vendors/js/ui/jquery.sticky.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="../../../app-assets/js/scripts/configs/horizontal-menu.min.js"></script>
        <script src="../../../app-assets/js/scripts/configs/vertical-menu-dark.min.js"></script>
        <script src="../../../app-assets/js/core/app-menu.min.js"></script>
        <script src="../../../app-assets/js/core/app.min.js"></script>
        <script src="../../../app-assets/js/scripts/components.min.js"></script>
        <script src="../../../app-assets/js/scripts/footer.min.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <!-- END: Page JS-->
    </form>
</body>
<!-- END: Body-->
</html>
